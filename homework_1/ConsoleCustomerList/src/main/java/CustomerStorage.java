import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class CustomerStorage {
    private final Map<String, Customer> storage;
    public static final Logger errorsLogger = LogManager.getLogger(CustomerStorage.class);

    public CustomerStorage() {
        storage = new HashMap<>();
    }

    public void addCustomer(String data) throws IncorrectComponentsException, WrongNumberFormatException,
            WrongEmailFormatException {

        final int INDEX_NAME = 0;
        final int INDEX_SURNAME = 1;
        final int INDEX_EMAIL = 2;
        final int INDEX_PHONE = 3;
        final String NUMBER_FORMAT_REGEX = "\\+[\\d]{1}[0-9]{10}";
        final String EMAIL_FORMAT_REGEX = "[A-Za-z._0-9A-Za-z]+@[A-Za-z]+\\.[comru]{2,3}";

        String[] components = data.split("\\s+");
        if (components.length != 4) {
            errorsLogger.log(Level.ERROR, "Возникла ошибка: длина строки больше или меньше 4");
            throw new IncorrectComponentsException("Неверный ввод. " +
                    "Правильный формат команды: add Василий Петров vasily.petrov@gmail.com +79215637722. \n" +
                    "Повторите ввод: ");
        }
        if (!components[INDEX_PHONE].matches(NUMBER_FORMAT_REGEX)) {
            errorsLogger.log(Level.ERROR,"Возникла ошибка: неверный формат номера");
            throw new WrongNumberFormatException("Неверный формат номера. Правильный формат: +79215637722. \n" +
                    "Повторите ввод: ");
        }
        if (!components[INDEX_EMAIL].matches(EMAIL_FORMAT_REGEX)) {
            errorsLogger.log(Level.ERROR,"Возникла ошибка: неверный формат электронного адреса");
            throw new WrongEmailFormatException("Неверный формат электронного адреса. " +
                    "Правильный формат: vasily.petrov@gmail.com. \n Повторите ввод: ");
        }
        String name = components[INDEX_NAME] + " " + components[INDEX_SURNAME];
        storage.put(name, new Customer(name, components[INDEX_PHONE], components[INDEX_EMAIL]));
    }

    public void listCustomers() {
        storage.values().forEach(System.out::println);
    }

    public void removeCustomer(String name) {
        storage.remove(name);
    }

    public Customer getCustomer(String name) {
        return storage.get(name);
    }

    public int getCount() {
        return storage.size();
    }
}

class IncorrectComponentsException extends Exception {
    public IncorrectComponentsException(String message) {
        super(message);
    }
}

class WrongNumberFormatException extends Exception {
    public WrongNumberFormatException(String message) {
        super(message);
    }
}

class WrongEmailFormatException extends Exception {
    public WrongEmailFormatException(String message) {
        super(message);
    }
}