import core.Line;
import core.Station;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.*;

public class RouteCalculatorTest extends TestCase {
    public StationIndex stationIndex;
    public RouteCalculator routeCalculator;
    public TreeSet<Station> stations;
    public Map<Station, TreeSet<Station>> connections;
    public Map<Integer, Line> numberToLine;
    public List<Station> route;

    @Override
    protected void setUp() throws Exception {
        stationIndex = new StationIndex();
        stations = new TreeSet<>();
        connections = new HashMap<>();
        numberToLine = new HashMap<>();
        route = new ArrayList<>();

        Line line1 = new Line(1, "Первая");
        Line line2 = new Line(2, "Вторая");
        Line line3 = new Line(3, "Третья");

        Station station1 = new Station("Бирюзовая", line1);
        Station station2 = new Station("Лиловая", line1);
        Station station3 = new Station("Петровская", line2);
        Station station4 = new Station("Васильевская", line2);
        Station station5 = new Station("Ежевичная", line3);
        Station station6 = new Station("Клубничная", line3);

        line1.addStation(station1);
        line1.addStation(station2);
        line2.addStation(station3);
        line2.addStation(station4);
        line3.addStation(station5);
        line3.addStation(station6);

        stations.add(station1);
        stations.add(station2);
        stations.add(station3);
        stations.add(station4);
        stations.add(station5);
        stations.add(station6);

        stationIndex.addLine(line1);
        stationIndex.addLine(line2);
        stationIndex.addLine(line3);

        stationIndex.addStation(station1);
        stationIndex.addStation(station2);
        stationIndex.addStation(station3);
        stationIndex.addStation(station4);
        stationIndex.addStation(station5);
        stationIndex.addStation(station6);

        numberToLine.put(1, line1);
        numberToLine.put(2, line2);
        numberToLine.put(3, line3);

        TreeSet<Station> connectionsColor = new TreeSet<>();
        connectionsColor.add(station5);
        TreeSet<Station> connectionsNames = new TreeSet<>();
        connectionsNames.add(station4);

        connections.put(station1, connectionsColor);
        connections.put(station6, connectionsNames);

        stationIndex.addConnection(new ArrayList<>((Arrays.asList(station1, station5))));
        stationIndex.addConnection(new ArrayList<>((Arrays.asList(station6, station4))));

        stationIndex.getConnectedStations(station1);
        stationIndex.getConnectedStations(station5);
        stationIndex.getConnectedStations(station6);
        stationIndex.getConnectedStations(station4);

        route.add(station2);
        route.add(station1);
        route.add(station5);
        route.add(station6);
        route.add(station4);
        route.add(station3);
    }

    @Test
    public void testGetShortestRoute() {
        routeCalculator = new RouteCalculator(stationIndex);

        List<Station> actual = routeCalculator.getShortestRoute(stationIndex.getStation("Бирюзовая"),
                stationIndex.getStation("Лиловая"));
        List<Station> expected = new ArrayList<>();
        expected.add(stationIndex.getStation("Бирюзовая"));
        expected.add(stationIndex.getStation("Лиловая"));
        assertNotNull(actual);
        assertEquals(expected, actual);

        List<Station> actual2 = routeCalculator.getShortestRoute(stationIndex.getStation("Лиловая"),
                stationIndex.getStation("Клубничная"));
        List<Station> expected2 = new ArrayList<>();
        expected2.add(stationIndex.getStation("Лиловая"));
        expected2.add(stationIndex.getStation("Бирюзовая"));
        expected2.add(stationIndex.getStation("Ежевичная"));
        expected2.add(stationIndex.getStation("Клубничная"));
        assertNotNull(actual2);
        assertEquals(expected2, actual2);

        List<Station> actual3 = routeCalculator.getShortestRoute(stationIndex.getStation("Лиловая"),
                stationIndex.getStation("Петровская"));
        List<Station> expected3 = new ArrayList<>();
        expected3.add(stationIndex.getStation("Лиловая"));
        expected3.add(stationIndex.getStation("Бирюзовая"));
        expected3.add(stationIndex.getStation("Ежевичная"));
        expected3.add(stationIndex.getStation("Клубничная"));
        expected3.add(stationIndex.getStation("Васильевская"));
        expected3.add(stationIndex.getStation("Петровская"));
        assertNotNull(actual3);
        assertEquals(expected3, actual3);
    }

    @Test
    public void testCalculateDuration() {
        double actual = RouteCalculator.calculateDuration(route);
        double expected = 14.5D;
        assertEquals("Длительность поездки: ", expected, actual);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
